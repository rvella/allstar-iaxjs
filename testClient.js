/**
 * IAX Client for Load Testing
 *
 * @author Rob Vella KK9ROB <me@robvella.com>
 * @copyright Copyright (c) 2020 AllStarLink, Inc
 * @license AGPL-3.0-or-later
 */

require('dotenv').config();

const Server = require('./lib/Server');
console.log('Connecting to',process.env.TEST_SERVER_IP);

const IaxClient = new (require('./lib/IaxClient'))({
    // Server we're connecting to
    address: process.env.TEST_SERVER_IP,
    port: process.env.TEST_SERVER_PORT
});
const Util = require('./lib/Util');
const colors = require('colors');
const { nodes } = require('./nodes.json');
const _ = require('lodash');

process.env.TEST_CLIENT = true;

Server.init((msg, info)=> {
    try {
        IaxClient.receiveMessage(msg, info);
    } catch (e) {
        console.error(e);
    }
}, 4570, async (server) => {
    IaxClient.server = server;
    
    setTimeout(() => {
        console.log('Registrations:', IaxClient.registrations);
        process.exit();
    }, 60000);

    for (const [k, node] of Object.entries(_.shuffle(nodes))) {
        IaxClient.sendRegReq(node.name, node.secret);
        await Util.timeout(20);
    }
});

