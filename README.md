# AllStarLink IAX Server

Multi-threaded IAX2 Registration Server. Performs IAX2 registrations for Asterisk clients.

## Authors
Rob Vella, KK9ROB <me@robvella.com>

## License
GNU Affero 3.0 or later

https://www.gnu.org/licenses/agpl-3.0.en.html

## Copyright
Copyright (C) 2020 AllStarLink, Inc
